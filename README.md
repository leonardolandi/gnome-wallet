Please take a look at <a href="https://gnomewallet.wordpress.com">https://gnomewallet.wordpress.com</a> for additional informations.

<h4>Features:</h4>
- Add, remove, edit expenses<br>
- Multiple archives where to store data<br>
- Password protection<br>
- Export into .pdf, .txt or sqlite .db formats<br>
- Backup and import from other archives or from file<br>
- Statistical graphs<br>

<h4>Installation:</h4>
Ensure these dependencies are installed: <i>python >= 3.0</i>, <i>Gtk+ >= 3.20</i>, <i>sqlite3</i>.<br>
Optional dependency: <i>enscript</i> (for "export into pdf" functionality).<br><br>
Then:
<pre>
git clone git://gitlab.com/leonardolandi/gnome-wallet.git
cd gnome-wallet
su
make install
</pre>

<h4>Installation (Archlinux):</h4>
<pre>
mkdir gnome-wallet
cd gnome-wallet
curl https://gitlab.com/leonardolandi/gnome-wallet/raw/master/PKGBUILD --output PKGBUILD
makepkg -si
</pre>
