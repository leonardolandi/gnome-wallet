# row_Category.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk,Gdk

class CategoryRow:

	def __init__(self,category,number,color):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/row_Category.glade")
		builder.set_translation_domain("wallet")

		self.child = builder.get_object("child")
		builder.get_object("category").set_text(category)
		builder.get_object("number").set_text(str(number))
		self.button = builder.get_object("button")
		self.button.set_relief(Gtk.ReliefStyle.NONE)
		self.button.set_rgba(Gdk.RGBA(color[0]/255,color[1]/255,color[2]/255,1))
