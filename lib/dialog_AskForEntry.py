# dialog_AskForEntry.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from locale import gettext as _

class AskForEntry:

	def __init__(self,main_window,name,password):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/dialog_AskForEntry.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		self.window = builder.get_object("window")
		self.alert_revealer = builder.get_object("alert_revealer")
		self.alert_label = builder.get_object("alert_label")
		self.entry = builder.get_object("entry")
		self.password_popover = builder.get_object("password_popover")

		self.window.set_transient_for(main_window)
		if password:
			self.window.set_title(_("Enter current password for") + " \"" + name + "\"")
			self.entry.set_placeholder_text(_("Enter password here"))
			self.entry.set_visibility(False)
			self.entry.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY,"dialog-password-symbolic")
		else:
			self.window.set_title(_("Rename archive") + " \"" + name + "\"")
			self.entry.set_placeholder_text(_("Enter here the new name"))

	def alert(self,text):
		self.alert_label.set_text(text)
		self.alert_revealer.set_reveal_child(True)
		self.entry.get_style_context().add_class("error")
		self.entry.grab_focus()

	def on_window_key_release_event(self,widget,event):
		if event.keyval == 65293: widget.emit("response",1)

	def on_entry_changed(self,widget):
		self.alert_revealer.set_reveal_child(False)
		self.entry.get_style_context().remove_class("error")

	def on_entry_icon_release(self,widget,position,event):
		if self.entry.get_visibility(): self.password_popover.get_children()[0].set_label(_("Hide password"))
		else: self.password_popover.get_children()[0].set_label(_("Show password"))
		self.password_popover.show_all()

	def on_password_popover_clicked(self,widget): self.entry.set_visibility(not self.entry.get_visibility())
		
