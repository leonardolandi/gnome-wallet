# row_Expense.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from functions import months

class ExpenseRow:

	def __init__(self,reason,amount,category,hiddencontent):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/row_Expense.glade")
		builder.set_translation_domain("wallet")

		self.row = builder.get_object("row")
		builder.get_object("reason").set_text(reason)
		builder.get_object("category").set_text(category)
		builder.get_object("date").set_text(str(hiddencontent[1]) + " " + months[hiddencontent[2]-1] + " " + str(hiddencontent[3]))
		color = "red" if amount[0] == "-" else "green"
		builder.get_object("amount").set_markup("<span foreground='" + color + "' >" + amount[1:] + "</span>")
		hiddenbox = builder.get_object("hiddenbox")
		for i in range(4): hiddenbox.get_children()[i].set_text(str(hiddencontent[i]))
		if len(category) > 0: builder.get_object("separator").set_visible(True)
		if color == "red": builder.get_object("sign").set_text("-")
		else: builder.get_object("sign").set_text("+")
