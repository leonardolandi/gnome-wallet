# tab_CakeGraph.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from math import pi,cos,sin
import cairo

class CakeGraph:

	def __init__(self,data = None):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/tab_CakeGraph.glade")

		self.box = builder.get_object("box")
		self.drawingarea = builder.get_object("drawingarea")
		self.drawingarea.connect("draw",self.on_drawingarea_draw)
		self.balance, self.surface, self.context = 0, None, self.drawingarea.get_style_context()
		self.import_data(data)

	def on_drawingarea_draw(self,widget,context):
		context.set_source_surface(self.surface,0,0)
		context.paint()

	def update_graph(self,categories):
		if self.surface is None: return
		self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,self.drawingarea.get_allocated_width(),self.drawingarea.get_allocated_height())
		angle, total = 0, 0		
		if self.balance is 0:
			for k in categories:
				if k != "_": total += categories[k][1]
		else: total = categories["_"][1]
		if total == 0:
			self.surface.flush()
			self.drawingarea.queue_draw()
			return
		context, width, height = cairo.Context(self.surface), self.surface.get_width(), self.surface.get_height()
		for k in sorted(categories, key=lambda x: abs(categories[x][1])):
			perc = categories[k][1]/total
			if k != "_" and perc > 0:
				color, radius = categories[k][3], height*2*0.95/5
				# draw cake slice
				context.set_source_rgb(color[0]/255,color[1]/255,color[2]/255)
				context.move_to(width/2,height/2)
				context.arc(width/2,height/2,radius,angle,angle + 2*pi*perc)
				context.fill()
				# draw line from slice to text
				teta = angle + pi*perc
				cost, sint = cos(teta), sin(teta)
				A = (width/2 + radius*cost, height/2 + radius*sint)
				B = (A[0] + 20*cost, A[1] + 20*sint)
				context.set_line_width(1)
				context.move_to(A[0],A[1])
				context.line_to(B[0],B[1])
				# draw text
				text = str(round(perc*100,2)) + "%"
				shift = 10 + 7*(len(text)-2) if cost < 0 else 0
				context.move_to(B[0] - shift,B[1] + 4 + 4*sint)
				context.show_text(text)
				context.stroke()
				# draw category selection
				if categories[k][2]:
					context.set_line_width(4)
					context.set_source_rgb(0,0,0)
					context.arc(width/2,height/2,radius,angle,angle + 2*pi*perc)
					context.stroke()
				angle += 2*pi*perc
		self.surface.flush()
		self.drawingarea.queue_draw()

	def export_data(self): return {
		"x_inf": self.x_inf,
		"x_sup": self.x_sup,
		"x_dist": self.x_dist,
		"y_dist": self.y_dist }

	def import_data(self,data):
		self.x_inf = data["x_inf"] # first date, date format
		self.x_sup = data["x_sup"] # last date, date format
		self.x_dist = data["x_dist"] # number of notches on the x axis
		self.y_dist = data["y_dist"] # max distance between amount values
