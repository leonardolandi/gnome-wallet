# dialog_NewArchive.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from locale import gettext as _

class NewArchive:

	def __init__(self,main_window,name):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/dialog_NewArchive.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		self.window = builder.get_object("window")
		self.alert_revealer = builder.get_object("alert_revealer")
		self.alert_label = builder.get_object("alert_label")
		self.name_entry = builder.get_object("name_entry")
		self.switch = builder.get_object("switch")
		self.password_revealer = builder.get_object("password_revealer")
		self.password_entry = builder.get_object("password_entry")
		self.repassword_entry = builder.get_object("repassword_entry")
		self.password_popover = builder.get_object("password_popover")
		self.repassword_popover = builder.get_object("repassword_popover")

		self.window.set_transient_for(main_window)
		if name is None:
			self.window.set_title(_("Create a new archive"))
			builder.get_object("ok_button").set_label(_("Create"))
			builder.get_object("name_box").set_visible(True)
		else:
			self.window.set_title(_("Set password of") + " \"" + name + "\"")
			builder.get_object("ok_button").set_label(_("Apply"))

	def alert(self,text,error):
		self.alert_label.set_text(text)
		self.alert_revealer.set_reveal_child(True)
		widget = [self.name_entry,self.password_entry,self.repassword_entry][error]
		widget.get_style_context().add_class("error")
		widget.grab_focus()

	def on_window_key_release_event(self,widget,event):
		if event.keyval == 65293 and not self.switch.is_focus(): widget.emit("response",1)

	def on_entry_changed(self,widget):
		self.alert_revealer.set_reveal_child(False)
		for wid in [self.name_entry,self.password_entry,self.repassword_entry]: wid.get_style_context().remove_class("error")

	def on_password_entry_icon_release(self,widget,position,event):
		if self.password_entry.get_visibility(): self.password_popover.get_children()[0].set_label(_("Hide password"))
		else: self.password_popover.get_children()[0].set_label(_("Show password"))
		self.password_popover.show_all()

	def on_repassword_entry_icon_release(self,widget,position,event):
		if self.repassword_entry.get_visibility(): self.repassword_popover.get_children()[0].set_label(_("Hide password"))
		else: self.repassword_popover.get_children()[0].set_label(_("Show password"))
		self.repassword_popover.show_all()

	def on_password_popover_clicked(self,widget): self.password_entry.set_visibility(not self.password_entry.get_visibility())

	def on_repassword_popover_clicked(self,widget): self.repassword_entry.set_visibility(not self.repassword_entry.get_visibility())

	def on_switch_state_set(self,widget,state):
		self.alert_revealer.set_reveal_child(False)
		self.password_revealer.set_reveal_child(state)
		if not state:
			for wid in [self.password_entry,self.repassword_entry]:
				wid.set_text("")
				wid.set_visibility(False)
