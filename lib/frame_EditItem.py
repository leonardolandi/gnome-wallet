# frame_EditItem.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk

class EditItem:

	def __init__(self,categories,date):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/frame_EditItem.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		self.frame = builder.get_object("frame")
		self.reason = builder.get_object("reason")
		self.amount = builder.get_object("amount")
		self.category = builder.get_object("category")
		self.combo = builder.get_object("combo")
		self.ok_button = builder.get_object("ok_button")
		self.popover = builder.get_object("popover")
		self.checkbutton = builder.get_object("checkbutton")
		self.calendar = builder.get_object("calendar")
		self.liststore = builder.get_object("liststore")
		self.calendar.select_day(date.day)
		self.calendar.select_month(date.month-1,date.year)
		for i in range(len(categories)): self.liststore.append([i+3,categories[i]])

	def on_entry_changed(self,widget): widget.get_style_context().remove_class("error")

	def on_calendar_button_toggled(self,widget): self.popover.set_visible(widget.get_active())

	def on_popover_closed(self,widget): widget.get_relative_to().set_active(False)

	def on_calendar_day_selected_double_click(self,widget): self.popover.set_visible(False)

	def on_checkbutton_toggled(self,widget): self.calendar.set_visible(not widget.get_active())

	def on_combo_changed(self,widget):
		if widget.get_active() > 0: self.category.set_visible(False)
		else:
			self.category.set_text("")
			self.category.set_visible(True)

	def alert(self,entry):
		entry.get_style_context().add_class("error")
		entry.grab_focus()
