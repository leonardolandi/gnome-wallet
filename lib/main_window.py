# main_window.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import cairo
from shutil import copyfile
from locale import gettext as _
from gi import require_version
require_version("Gtk","3.0")
from gi.repository import Gtk, Gdk

# user-defined libraries
import translation
from class_Archive import *
from functions import *
from dialog_About import About
from dialog_ArchiveChooser import ArchiveChooser
from dialog_AskForEntry import AskForEntry
from dialog_ExportIntoFile import ExportIntoFile
from dialog_ImportFromFile import ImportFromFile
from dialog_Invalid import InvalidDialog
from dialog_NewArchive import NewArchive
from dialog_RemoveArchive import RemoveArchive
from frame_AddItem import AddItem
from frame_EditItem import EditItem
from frame_RemoveItem import RemoveItem
from frame_Unselected import Unselected
from row_Archive import ArchiveRow
from row_Category import CategoryRow
from row_Expense import ExpenseRow
from tab_CakeGraph import CakeGraph
from tab_LineGraph import LineGraph
from tab_DotsGraph import DotsGraph

class MainWindow(Gtk.ApplicationWindow):

	def __init__(self,application):

		Gtk.Window.__init__(self, application=application)
		builder = Gtk.Builder()
		builder.add_from_file(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/glade/main_window.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		# HEADER BAR
		self.header_bar = builder.get_object("header_bar")
		self.archives_button = builder.get_object("archives_button")
		self.graph_button = builder.get_object("graph_button")

		# LEFT POPOVER
		self.left_popover = builder.get_object("left_popover")
		self.search_in_archives = builder.get_object("search_in_archives")
		self.archives_listbox = builder.get_object("archives_listbox")

		# RIGHT POPOVER
		self.right_popover = builder.get_object("right_popover")
		self.menu_settings = builder.get_object("menu_settings")
		self.menu_backup = builder.get_object("menu_backup")
		self.menu_view = builder.get_object("menu_view")
		self.menu_close = builder.get_object("menu_close")
		self.radio_box = builder.get_object("radio_box")

		# CENTRAL BODY
		self.home_page_box = builder.get_object("home_page_box")
		self.main_box = builder.get_object("main_box")
		self.graph_revealer = builder.get_object("graph_revealer")
		self.calendar_popover = builder.get_object("calendar_popover")

		# SEARCH BAR
		self.search_bar = builder.get_object("search_bar")
		self.search_togglebutton = builder.get_object("search_togglebutton")
		self.checkbutton_revealer = builder.get_object("checkbutton_revealer")
		self.checkbutton_reason = builder.get_object("checkbutton_reason")
		self.checkbutton_amount = builder.get_object("checkbutton_amount")
		self.checkbutton_category = builder.get_object("checkbutton_category")
		self.checkbutton_date = builder.get_object("checkbutton_date")

		# LEFT PANE
		self.total_label = builder.get_object("total_label")
		self.expenses_listbox = builder.get_object("expenses_listbox")
		self.item_new = builder.get_object("item_new")
		self.item_edit = builder.get_object("item_edit")
		self.item_remove = builder.get_object("item_remove")
		self.item_revealer = builder.get_object("item_revealer")

		# RIGHT PANE
		self.notebook = builder.get_object("notebook")
		self.box1 = builder.get_object("box1")
		self.box2 = builder.get_object("box2")
		self.box3 = builder.get_object("box3")
		self.time_unit_box = builder.get_object("time_unit_box")
		self.balance_box = builder.get_object("balance_box")
		self.time_unit_combo = builder.get_object("time_unit_combo")
		self.balance_combo = builder.get_object("balance_combo")
		self.period_start_button = builder.get_object("period_start_button")
		self.period_end_button = builder.get_object("period_end_button")
		self.categories_flowbox = builder.get_object("categories_flowbox")

		# GLOBALS
		self.archives_folder, self.current_archive = os.getenv("HOME") + "/.wallet", None
		self.tab, self.block_signals = None, False
		self.expenses, self.categories = (), {}
		
		# ADD ACCELERATORS
		accelerators = Gtk.AccelGroup()
		self.add_accel_group(accelerators)
		accelerators.connect(Gdk.keyval_from_name("Q"), Gdk.ModifierType.CONTROL_MASK, 0, self.quit_all)
		accelerators.connect(Gdk.keyval_from_name("O"), Gdk.ModifierType.CONTROL_MASK, 0, self.open_archive)
		accelerators.connect(Gdk.keyval_from_name("F"), Gdk.ModifierType.CONTROL_MASK, 0, self.search_item)
		accelerators.connect(Gdk.keyval_from_name("N"), Gdk.ModifierType.CONTROL_MASK, 0, self.new_item)

		# DRAW
		self.set_default_size(10,500)
		self.header_bar.set_show_close_button(True)
		self.set_titlebar(self.header_bar)
		self.set_gravity(Gdk.Gravity.CENTER)
		self.add(builder.get_object("window_box"))
		self.connect("destroy",self.on_window_destroy)

# ACCELERATORS FUNCTIONS

	def quit_all(self,accel_group,acceleratable,keyval,modifier): exit(0)
	
	def open_archive(self,accel_group,acceleratable,keyval,modifier): self.archives_button.set_active(True)
	
	def search_item(self,accel_group,acceleratable,keyval,modifier):
		if self.search_bar.is_visible(): self.search_bar.grab_focus()
	
	def new_item(self,accel_group,acceleratable,keyval,modifier):
		if self.item_new.is_visible(): self.item_new.set_active(True)

# HEADER BAR SIGNALS

	def on_window_destroy(self,widget): self.block_signals = True

	def on_archives_button_toggled(self,widget):
		if widget.get_active():
			self.update_archives_listbox(self.archives_listbox,False)
			self.left_popover.set_visible(True)

	def on_graph_button_toggled(self,widget):
		if widget.get_active():
			self.show_graph_first_time()
			self.graph_revealer.show()
			self.graph_revealer.set_reveal_child(True)
		else:
			self.graph_revealer.set_reveal_child(False)
			self.graph_revealer.hide()
			self.resize(10,self.get_size()[1])

	def on_top_button_toggled(self,widget): self.right_popover.set_visible(widget.get_active())

# LEFT POPOVER SIGNALS

	def on_left_popover_closed(self,widget):
		widget.get_relative_to().set_active(False)
		self.search_in_archives.set_text("")

	def on_search_in_archives_changed(self,widget):
		if self.left_popover.get_visible():
			for row in self.archives_listbox.get_children(): self.archives_listbox.remove(row)
			namelist = []
			for filename in sorted(os.listdir(self.archives_folder)):
				filename = filename.replace("_"," ")
				if filename.endswith(".db.gpg"): namelist.append((filename[:-7],True))
				elif filename.endswith(".db"): namelist.append((filename[:-3],False))
			text = widget.get_text().lower()
			for filename in namelist:
				if text in filename[0].lower(): self.archives_listbox.insert(ArchiveRow(filename[0],filename[1]).row,-1)
			if len(self.archives_listbox.get_children()) > 0: self.archives_listbox.select_row(self.archives_listbox.get_children()[0])

	def on_search_in_archives_activate(self,widget):
		if len(self.archives_listbox.get_children()) > 0: self.on_archives_listbox_row_activated(self.archives_listbox,self.archives_listbox.get_selected_row())

	def on_archives_listbox_row_activated(self,widget,row):
		self.left_popover.set_visible(False)
		name, password = row.get_children()[0].get_children()[0].get_text(), ""
		if row.get_children()[0].get_children()[1].get_visible(): password = self.confirm_password(name)
		if password is not False:
			archive = Archive(self.archives_folder + "/" + name.replace(" ","_") + ".db",name,password)
			expenses = archive.select()
			if expenses is False: self.invalid_dialog(name); return
			self.block_signals = True
			for wid in [self.menu_settings,self.menu_backup,self.menu_view,self.menu_close]: wid.set_sensitive(True)
			for wid in [self.radio_box.get_children()[0],self.checkbutton_reason,self.checkbutton_amount,self.checkbutton_category,self.checkbutton_date]: wid.set_active(True)
			self.search_togglebutton.set_active(False)
			self.search_bar.set_text("")
			self.header_bar.set_subtitle(_("Archive: ") + name)
			self.current_archive, self.expenses = archive, expenses
			self.update_expenses_listbox()
			self.graph_button.set_visible(True)
			self.categories.clear()
			self.categories["_"] = [len(self.expenses),None,True]
			for item in self.expenses:
				if item[3] not in self.categories: self.categories[item[3]] = [1,(),False]
				else: self.categories[item[3]][0] += 1
			if self.graph_button.get_active(): self.show_graph_first_time()
			self.home_page_box.set_visible(False)
			self.main_box.set_visible(True)
			self.block_signals = False

	def on_menu_new_clicked(self,widget):
		dialog = NewArchive(self,None)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); break
			name, password, repassword = dialog.name_entry.get_text(), dialog.password_entry.get_text(), dialog.repassword_entry.get_text()
			if len(name) is 0: dialog.alert(_("Enter a name"),0)
			elif name[0] == " ": dialog.alert(_("Name can't begin with space character"),0)
			elif any((name.replace(" ","_") + suffix) in os.listdir(self.archives_folder) for suffix in [".db",".db.gpg"]): dialog.alert(_("Archive with this name already exists"),0)
			elif not all((char in legal_chars) for char in name): dialog.alert(_("Special character inside name not permitted"),0)
			elif not all((char in legal_chars) for char in password): dialog.alert(_("Special character inside password not permitted"),1)
			elif dialog.password_revealer.get_reveal_child() and len(password) is 0: dialog.alert(_("Enter a password"),1)
			elif dialog.password_revealer.get_reveal_child() and password != repassword: dialog.alert(_("Passwords don't match"),2)
			else:
				new_archive_file(self.archives_folder + "/" + name.replace(" ","_") + ".db",password)
				self.search_in_archives.set_text("")
				self.update_archives_listbox(self.archives_listbox,False)
				dialog.window.destroy(); break

	def on_menu_import_clicked(self,widget):
		dialog = ImportFromFile(self)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); break
			filepath, basename, archives = dialog.window.get_filename(), None, os.listdir(self.archives_folder)
			try: basename = os.path.basename(filepath)
			except AttributeError: pass
			if basename is None: dialog.alert(_("No file selected"))
			elif basename.replace(" ","") in [".db",".db.gpg"]: dialog.alert(_("Selected archive has no name"))
			elif basename[-3:] != ".db" and basename[-7:] != ".db.gpg": dialog.alert(_("Folders can't be selected"))
			elif basename.replace(" ","_") in archives or basename.replace(" ","_") + ".gpg" in archives: dialog.alert(_("Archive with this name already exists"))
			elif basename[-4:] == ".gpg" and basename[:-4].replace(" ","_") in archives: dialog.alert(_("Archive with this name already exists"))
			else:
				copyfile(filepath,self.archives_folder + "/" + basename.replace(" ","_"))
				self.search_in_archives.set_text("")
				self.update_archives_listbox(self.archives_listbox,False)
				dialog.window.destroy(); break

	def on_menu_remove_clicked(self,widget):
		dialog = ArchiveChooser(self,True)
		current_name = self.current_archive.name if self.current_archive else None
		self.update_archives_listbox(dialog.listbox,True,current_name)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); break
			row = dialog.listbox.get_selected_row()
			if row:
				name, suffix, confirm = row.get_children()[0].get_children()[0].get_text(), ".db", True
				if row.get_children()[0].get_children()[1].get_visible():
					confirm = self.confirm_password(name)
					suffix += ".gpg"
				if confirm:
					os.remove(self.archives_folder + "/" + name.replace(" ","_") + suffix)
					self.search_in_archives.set_text("")
					self.update_archives_listbox(self.archives_listbox,False)
					dialog.window.destroy(); break

# RIGHT POPOVER SIGNALS

	def on_right_popover_closed(self,widget):
		widget.get_relative_to().set_active(False)
		widget.open_submenu("archive_menu")

	def on_menu_close_clicked(self,widget):
		self.current_archive = None
		for wid in [self.menu_settings,self.menu_backup,self.menu_view,self.menu_close]: wid.set_sensitive(False)
		for wid in [self.main_box,self.graph_button]: wid.set_visible(False)
		self.header_bar.set_subtitle("")
		self.home_page_box.set_visible(True)

	def on_menu_info_clicked(self,widget):
		dialog = About(self)
		if dialog.window.run(): dialog.window.destroy()

	def on_menu_quit_clicked(self,widget): exit(0)

# RIGHT POPOVER SIGNALS > SETTINGS SUBMENU

	def on_menu_rename_clicked(self,widget):
		confirm = True
		if len(self.current_archive.password) > 0: confirm = self.confirm_password(self.current_archive.name)
		if confirm:
			dialog = AskForEntry(self,self.current_archive.name,False)
			while True:
				if dialog.window.run() is 0: dialog.window.destroy(); break
				new_name = dialog.entry.get_text()
				if len(new_name) is 0: dialog.alert(_("Enter a name"))
				elif new_name[0] == " ": dialog.alert(_("Name can't begin with space character"))
				elif not all((char in legal_chars) for char in new_name): dialog.alert(_("Special character not permitted"))
				elif new_name == self.current_archive.name: dialog.alert(_("The new name is the same as the previous one"))
				elif any((new_name.replace(" ","_") + suffix) in os.listdir(self.archives_folder) for suffix in [".db",".db.gpg"]): dialog.alert(_("Archive already exists"))
				else:
					suffix = "" if len(self.current_archive.password) is 0 else ".gpg"
					new_filepath = self.archives_folder + "/" + new_name.replace(" ","_") + ".db"
					os.rename(self.current_archive.filepath + suffix,new_filepath + suffix)
					self.current_archive.rename(new_filepath,new_name)
					self.header_bar.set_subtitle(_("Archive: ") + self.current_archive.name)
					dialog.window.destroy(); break

	def on_menu_reset_clicked(self,widget):
		confirm = True
		if len(self.current_archive.password) > 0: confirm = self.confirm_password(self.current_archive.name)
		if confirm:
			dialog = NewArchive(self,self.current_archive.name)
			while True:
				if dialog.window.run() is 0: dialog.window.destroy(); break
				protected, password, repassword = dialog.password_revealer.get_reveal_child(), dialog.password_entry.get_text(), dialog.repassword_entry.get_text()
				if protected and len(password) is 0: dialog.alert(_("Enter password"),1)
				elif not all((char in legal_chars) for char in password): dialog.alert(_("Special character not permitted"),1)
				elif password != repassword: dialog.alert(_("Passwords don't match"),2)
				else:
					self.current_archive.reset_password(password)
					dialog.window.destroy(); break

	def on_menu_remove_this_clicked(self,widget):
		confirm = True
		if len(self.current_archive.password) > 0: confirm = self.confirm_password(self.current_archive.name)
		if confirm:
			dialog = RemoveArchive(self,self.current_archive.name,None)
			if dialog.window.run() is 0: dialog.window.destroy(); return
			suffix = "" if len(self.current_archive.password) is 0 else ".gpg"
			os.remove(self.current_archive.filepath + suffix)
			self.on_menu_close_clicked(None)
			dialog.window.destroy()

# RIGHT POPOVER SIGNALS > BACKUP SUBMENU

	def on_menu_import_data_clicked(self,widget):
		dialog = ArchiveChooser(self,False)
		self.update_archives_listbox(dialog.listbox,True,self.current_archive.name)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); break
			row = dialog.listbox.get_selected_row()
			if row:
				name, suffix, confirm = row.get_children()[0].get_children()[0].get_text(), ".db", True
				if row.get_children()[0].get_children()[1].get_visible():
					confirm = self.confirm_password(name)
					suffix += ".gpg"
				if confirm:
					password = "" if confirm is True else confirm
					rows = Archive(self.archives_folder + "/" + name.replace(" ","_") + ".db",name,password).select()
					if rows is False: self.invalid_dialog(name)
					elif not self.current_archive.insert([(x[1],x[2],x[3],x[4],x[5],x[6]) for x in rows],True): self.invalid_dialog(self.current_archive.name)
					else:
						if dialog.checked: os.remove(self.archives_folder + "/" + name.replace(" ","_") + suffix)
						self.expenses += rows
						self.update_expenses_listbox()
						self.categories["_"][0] += len(rows)
						changed = ["_"]
						for row in rows:
							if row[3] in self.categories: self.categories[row[3]][0] += 1
							else: self.categories[row[3]] = [1,(),False]
							if row[3] not in changed: changed.append(row[3])
						if self.graph_button.get_active(): self.update_graph()
						dialog.window.destroy(); break

	def on_menu_backup_clicked(self,widget): self.export_into(".db")

	def on_menu_export_txt_clicked(self,widget): self.export_into(".txt")

	def on_menu_export_pdf_clicked(self,widget): self.export_into(".pdf")

# RIGHT POPOVER SIGNALS > VIEW SUBMENU

	def on_radio_button_toggled(self,widget):
		if self.block_signals: return
		if not widget.get_active(): return
		self.update_expenses_listbox()
		self.right_popover.set_visible(False)

# MAIN LIST SIGNALS

	def on_search_bar_changed(self,widget):
		if self.block_signals: return
		self.search_togglebutton.set_active(False)
		self.update_expenses_listbox()

	def on_search_togglebutton_toggled(self,widget): self.checkbutton_revealer.set_reveal_child(widget.get_active())

	def on_checkbutton_toggled(self,widget):
		if self.block_signals or len(self.search_bar.get_text()) is 0: return
		self.update_expenses_listbox()

	def on_expenses_listbox_selected_rows_changed(self,widget):
		if self.item_edit.get_active(): self.on_item_edit_toggled(self.item_edit)
		elif self.item_remove.get_active(): self.on_item_remove_toggled(self.item_remove)

# ADD ITEM SIGNALS

	def on_item_new_toggled(self,widget):
		if self.block_signals: return
		if widget.get_active():
			# clean the revealer content
			for wid in [self.item_edit,self.item_remove]: wid.set_active(False)
			revealer_content = self.item_revealer.get_children()
			if len(revealer_content) is 1: self.item_revealer.remove(revealer_content[0])
			# get the list of categories
			categories = sorted(self.categories, key=lambda x: (x != "_",x.lower()))
			categories.remove("_")
			if "" in categories: categories.remove("")
			# fill the revealer with content
			frame = AddItem(categories,datetime.date.today())
			frame.ok_button.connect("clicked",self.on_item_new_clicked,frame)
			self.item_revealer.add(frame.frame)
			frame.reason.grab_focus()
		self.item_revealer.set_reveal_child(widget.get_active())

	def on_item_new_clicked(self,widget,frame):
		reason, amount, category, active = frame.reason.get_text(), frame.amount.get_text(), frame.category.get_text(), frame.combo.get_active()
		if len(reason) is 0: frame.alert(frame.reason)
		elif not validate_amount(amount): frame.alert(frame.amount)
		elif active is 0 and (len(category) is 0 or category[0] == " " or any ((item[1] == category) for item in frame.liststore)): frame.alert(frame.category)
		else:
			widget.set_sensitive(False)
			# get values to add
			date = frame.calendar.get_date()
			if active is 1: category = ""
			elif active > 1: category = frame.liststore[active][1]
			values = (reason, adjust_amount(amount), category, date[0], date[1]+1, date[2])
			# apply operation
			res = self.current_archive.insert(values)
			if not res: self.invalid_dialog(self.current_archive.name); return
			# update categories
			self.categories["_"][0] += 1
			if category in self.categories: self.categories[category][0] += 1
			else: self.categories[category] = [1,(),False]
			# update expenses
			self.expenses += (res,values[0],values[1],values[2],values[3],values[4],values[5]),
			self.update_expenses_listbox()
			# update graph
			if self.graph_button.get_active(): self.update_graph()
			self.item_new.set_active(False)

# EDIT ITEM SIGNALS

	def on_item_edit_toggled(self,widget):
		if self.block_signals: return
		if widget.get_active():
			# clean the revealer content
			for wid in [self.item_new,self.item_remove]: wid.set_active(False)
			revealer_content = self.item_revealer.get_children()
			if len(revealer_content) is 1: self.item_revealer.remove(revealer_content[0])
			if len(self.expenses_listbox.get_selected_rows()) is 0: self.item_revealer.add(Unselected(_("Select from the list the items to be edited")).frame)
			else:
				# get the list of categories
				categories = sorted(self.categories, key=lambda x: (x != "_",x.lower()))
				categories.remove("_")
				if "" in categories: categories.remove("")
				# the revealer with content
				frame = EditItem(categories,datetime.date.today())
				frame.ok_button.connect("clicked",self.on_item_edit_clicked,frame)
				self.item_revealer.add(frame.frame)
		self.item_revealer.set_reveal_child(widget.get_active())

	def on_item_edit_clicked(self,widget,frame):
		reason, amount, category, active = frame.reason.get_text(), frame.amount.get_text(), frame.category.get_text(), frame.combo.get_active()
		if len(amount) > 0 and not validate_amount(amount): frame.alert(frame.amount)
		elif active is 0 and (len(category) is 0 or category[0] == " " or any ((item[1] == category) for item in frame.liststore)): frame.alert(frame.category)
		else:
			widget.set_sensitive(False)
			# get values to edit
			values = {}
			if len(reason) > 0: values["reason"] = reason
			if len(amount) > 0: values["amount"] = adjust_amount(amount)
			if active is 0: values["category"] = category
			elif active is 2: values["category"] = ""
			elif active > 2: values["category"] = frame.liststore[active][1]
			if not frame.checkbutton.get_active():
				date = frame.calendar.get_date()
				values["year"] = date[0]
				values["month"] = date[1] + 1
				values["day"] = date[2]
			# apply operation
			if len(values) is 0: self.item_edit.set_active(False); return
			ids = tuple(int(row.get_children()[0].get_children()[0].get_children()[0].get_text()) for row in self.expenses_listbox.get_selected_rows())
			if not self.current_archive.update(values,ids): self.invalid_dialog(self.current_archive.name); return
			# update expenses
			old_categories, expenses = {}, list(self.expenses)
			for i in range(len(expenses)):
				if expenses[i][0] in ids:
					cat = expenses[i][3]
					if cat in old_categories: old_categories[cat] += 1
					else: old_categories[cat] = 1
					keys = ("reason","amount","category","year","month","day")
					exp = list(expenses[i])
					for j in range(len(keys)):
						if keys[j] in values: exp[j+1] = values[keys[j]]
					expenses[i] = tuple(exp)
			self.expenses = tuple(expenses)
			self.update_expenses_listbox()
			# update categories
			if "category" in values:
				for key in old_categories:
					self.categories[key][0] -= old_categories[key]
					if self.categories[key][0] is 0: del self.categories[key]
				if values["category"] in self.categories: self.categories[values["category"]][0] += len(ids)
				else: self.categories[values["category"]] = [len(ids),(),False]
			# update graph
			if self.graph_button.get_active(): self.update_graph()
			self.item_edit.set_active(False)

# REMOVE ITEM SIGNALS

	def on_item_remove_toggled(self,widget):
		if self.block_signals: return
		if widget.get_active():
			# clean the revealer content
			for wid in [self.item_new,self.item_edit]: wid.set_active(False)
			revealer_content, number = self.item_revealer.get_children(), len(self.expenses_listbox.get_selected_rows())
			if len(revealer_content) is 1: self.item_revealer.remove(revealer_content[0])
			if number is 0: self.item_revealer.add(Unselected(_("Select from the list the items to be removed")).frame)
			else:
				# fill the revealer content
				frame = RemoveItem(number)
				frame.ok_button.connect("clicked",self.on_item_remove_clicked)
				self.item_revealer.add(frame.frame)
		self.item_revealer.set_reveal_child(widget.get_active())

	def on_item_remove_clicked(self,widget):
		# apply operation
		ids = tuple(int(row.get_children()[0].get_children()[0].get_children()[0].get_text()) for row in self.expenses_listbox.get_selected_rows())
		if not self.current_archive.delete(ids): self.invalid_dialog(self.current_archive.name); return
		widget.set_sensitive(False)
		# update categories
		old_categories = {}
		for exp in self.expenses:
			if exp[0] in ids:
				cat = exp[3]
				if cat in old_categories: old_categories[cat] += 1
				else: old_categories[cat] = 1
		for key in old_categories:
			self.categories[key][0] -= old_categories[key]
			if self.categories[key][0] is 0: del self.categories[key]
		self.categories["_"][0] -= len(ids)
		# update expenses
		self.expenses = tuple(x for x in self.expenses if x[0] not in ids)
		self.update_expenses_listbox()
		# update graph
		if self.graph_button.get_active(): self.update_graph()
		self.item_remove.set_active(False)

# NON-SIGNAL FUNCTIONS THAT RUN A DIALOG

	def confirm_password(self,name):
		dialog = AskForEntry(self,name,True)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); return False
			password, filepath = dialog.entry.get_text(), self.archives_folder + "/" + name.replace(" ","_") + ".db"
			if len(password) is 0: dialog.alert(_("Enter a password"))
			elif not all((char in legal_chars) for char in password): dialog.alert(_("Special character not permitted"))
			elif not match_archive_password(filepath,password): dialog.alert(_("Incorrect password"))
			else: dialog.window.destroy(); return password

	def invalid_dialog(self,name,packages=0):
		dialog = None
		if packages is 0: dialog = InvalidDialog(self,_("Archive") + " \"" + name + "\" " + _("is invalid or corrupted"))
		elif packages is 1: dialog = InvalidDialog(self,_("Command") + " \"" + name + "\" " + _("seems to be absent on your system.\nExport into PDF functionality will be available after installing the right package for your distribution."))
		else: dialog = InvalidDialog(self,_("Commands") + " \"" + name[0] + "\" " + _("and") + " \"" + name[1] + "\" " + _("seem to be absent on your system.\nExport into PDF functionality will be available after installing the right packages for your distribution."))
		if dialog.window.run(): dialog.window.destroy()

	def export_into(self,ext):
		iconv, enscript = os.path.isfile("/usr/bin/iconv"), os.path.isfile("/usr/bin/enscript")
		if ext == ".pdf" and not iconv and not enscript: dialog = self.invalid_dialog(["iconv","enscript"],2); return
		elif ext == ".pdf" and not iconv: dialog = self.invalid_dialog("iconv",1); return
		elif ext == ".pdf" and not enscript: dialog = self.invalid_dialog("enscript",1); return
		dialog = ExportIntoFile(self,self.current_archive.name + ext)
		while True:
			if dialog.window.run() is 0: dialog.window.destroy(); break
			if ext == ".db" and len(self.current_archive.password) > 0: ext += ".gpg"
			folderpath, basename = dialog.window.get_filename(), self.current_archive.name + ext
			if folderpath is None: dialog.alert(_("Choose a destination folder"))
			else:
				overwrite = True
				if basename in os.listdir(folderpath):
					overwrite_dialog = RemoveArchive(self,None,basename)
					if overwrite_dialog.window.run() is 0: overwrite = False;
					overwrite_dialog.window.destroy()
				if overwrite:
					try:
						output = folderpath + "/" + basename
						if ext in [".db",".db.gpg"]: copyfile(self.archives_folder + "/" + basename.replace(" ","_"),output)
						elif ext == ".txt": export_into_txt(self.expenses,output)
						else:
							txt_file = self.archives_folder + "/" + self.current_archive.name.replace(" ","_") + ".txt"
							export_into_txt(self.expenses,txt_file)
							check_output("cat " + txt_file + " | iconv -c -f UTF-8 -t latin1 | enscript -b '" + self.current_archive.name + "' -o '" + output + "'", shell=True)
							os.remove(txt_file)
						dialog.window.destroy(); break
					except PermissionError: dialog.alert(_("Write permission denied on selected folder"))

# GRAPH COMMANDS SIGNALS

	def on_notebook_switch_page(self,widget,child,page):
		if self.block_signals: return
		self.block_signals = True
		# save the old parameters
		olddata = self.tab.export_data()
		# set visibility of bottom widgets
		self.time_unit_box.set_visible(page < 2)
		self.balance_box.set_visible(page > 0)
		# create a new tab object
		if page is 0: self.tab = LineGraph(olddata)
		elif page is 1: self.tab = DotsGraph(olddata)
		else: self.tab = CakeGraph(olddata)
		# fill up the content
		box = [self.box1,self.box2,self.box3][page]
		children = box.get_children()
		if len(children) > 0: box.remove(children[0])
		box.add(self.tab.box)
		# connect event signals
		self.tab.drawingarea.connect("configure-event",self.on_drawingarea_configure_event)
		if page < 2:
			self.tab.drawingarea.connect("motion_notify_event",self.on_drawingarea_motion_notify_event)
			self.tab.drawingarea.connect("leave_notify_event",self.on_drawingarea_leave_notify_event)
		# draw
		self.update_graph(page)
		self.block_signals = False

	def on_time_unit_combo_changed(self,widget):
		if self.block_signals: return
		self.update_graph()

	def on_balance_combo_changed(self,widget):
		if self.block_signals: return
		self.tab.balance = widget.get_active()
		self.update_graph()
		
	def on_calendar_button_toggled(self,widget):
		if widget.get_active():
			date = widget.get_label().split("-")
			self.calendar_popover.set_relative_to(widget)
			self.calendar_popover.get_children()[0].select_day(int(date[0]))
			self.calendar_popover.get_children()[0].select_month(int(date[1])-1,int(date[2]))
		self.calendar_popover.set_visible(widget.get_active())

	def on_calendar_double_click(self,widget): widget.get_parent().set_visible(False)

	def on_calendar_key_release_event(self,widget,event):
		if event.keyval == 65293: widget.get_parent().set_visible(False)

	def on_calendar_popover_closed(self,widget):
		button, date = widget.get_relative_to(), widget.get_children()[0].get_date()
		button.set_active(False)
		button.set_label(adjust_date(date[0],date[1]+1,date[2]))
		if self.graph_button.get_active(): self.update_graph()

	def on_categories_flowbox_selected_children_changed(self,widget):
		if self.block_signals: return
		first = True
		for child in widget.get_children():
			category = "_" if first else child.get_children()[0].get_children()[1].get_children()[0].get_text()
			if category not in self.categories: widget.remove(child)
			else:
				self.categories[category][2] = child.is_selected()
				color = child.get_children()[0].get_children()[0].get_rgba()
				self.categories[category][3] = (round(color.red*255),round(color.green*255),round(color.blue*255))
			first = False
		self.update_graph()

# NOTEBOOK TAB OBJECTS SIGNALS

	def on_drawingarea_configure_event(self,widget,event):
		if self.block_signals: return
		self.tab.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,widget.get_allocated_width(),widget.get_allocated_height())
		self.tab.update_graph(self.categories)

	def on_drawingarea_motion_notify_event(self,widget,event):
		if self.block_signals: return
		if self.tab.x_dist is 0 or self.tab.y_dist is 0: return
		width, height = self.tab.surface.get_width(), self.tab.surface.get_height()
		x_div, y_div = width/self.tab.x_dist, height/self.tab.y_dist
		# set x marker
		shift = int(round(event.x/x_div))
		time_unit = self.time_unit_combo.get_active()
		date = next_item(self.tab.x_inf,time_unit,shift)
		self.tab.x_label.set_text(get_label_date(date,time_unit))
		# set y marker
		text = (height - event.y)/(0.95*y_div) if self.notebook.get_current_page() is 1 and self.tab.balance > 0 else (height/2 - event.y)/(0.95*y_div)
		text = str(int(round(text)))
		if "." in text and len(text.split(".")[1]) is 1: text += "0"
		self.tab.y_label.set_text(text)
	
	def on_drawingarea_leave_notify_event(self,widget,event):
		for wid in [self.tab.x_label,self.tab.y_label]: wid.set_text("")

# NON-SIGNAL UPDATE FUNCTIONS

	def update_color(self,widget,category):
		color = widget.get_rgba()
		self.categories[category][3] = (round(color.red*255),round(color.green*255),round(color.blue*255))
		self.tab.update_graph(self.categories)

	def update_archives_listbox(self,listbox,dialog,excluded=None):
		for row in listbox.get_children(): listbox.remove(row)
		for filename in sorted(os.listdir(self.archives_folder), key=lambda x: x.lower()):
			filename = filename.replace("_"," ")
			if filename.endswith(".db.gpg") and filename[:-7] != excluded: listbox.insert(ArchiveRow(filename[:-7],True).row,-1)
			elif filename.endswith(".db") and filename[:-3] != excluded: listbox.insert(ArchiveRow(filename[:-3],False).row,-1)
		if dialog and len(listbox.get_children()) is 0: listbox.hide()
		if len(listbox.get_children()) > 0: listbox.select_row(listbox.get_children()[0])

	def update_expenses_listbox(self):
		radios, order = self.radio_box.get_children(), 0
		while not radios[order].get_active(): order += 1
		self.expenses = sort_list_by_order(self.expenses,order)
		for row in self.expenses_listbox.get_children(): self.expenses_listbox.remove(row)
		total, search = 0, self.search_bar.get_text().replace("\'","\'\'").lower()
		reason, amount, category, date = self.checkbutton_reason.get_active(), self.checkbutton_amount.get_active(), self.checkbutton_category.get_active(), self.checkbutton_date.get_active()
		for item in self.expenses:
			if validate_search(search,item,reason,amount,category,date):
				total += float(item[2])
				self.expenses_listbox.insert(ExpenseRow(item[1],item[2],item[3],(item[0],item[6],item[5],item[4])).row,-1)
		color = "red" if round(total,2) < 0 else "green"
		self.total_label.set_markup("<span foreground='" + color + "' >" + adjust_amount(str(round(total,2))) + "</span>")
		for widget in [self.item_new,self.item_edit,self.item_remove]: widget.set_active(False)
		for wid in [self.item_edit,self.item_remove]: wid.set_sensitive(len(self.expenses) > 0)

	# refreshes the graph according to the parameters
	def update_graph(self,page = None):
		self.block_signals = True
		if page is None: page = self.notebook.get_current_page()
		# update x_inf and x_sup
		start, end, time_unit = self.period_start_button.get_label().split("-"), self.period_end_button.get_label().split("-"), self.time_unit_combo.get_active()
		start, end = get_first_day(int(start[2]),int(start[1]),int(start[0]),time_unit), get_first_day(int(end[2]),int(end[1]),int(end[0]),time_unit)
		self.tab.x_inf, self.tab.x_sup = start, end
		# update x_dist
		iterator, dist = start, 0
		while iterator < end:
			dist += 1
			iterator = next_item(iterator,time_unit)
		self.tab.x_dist = dist
		# update the variable self.categories storing all the data
		expenses = sort_list_by_order(self.expenses,1)
		if page < 2 and (self.tab.x_dist is 0 or len(expenses) is 0):
			for k in self.categories: self.categories[k][1] = ()
		elif page is 2 and len(expenses) is 0:
			for k in self.categories: self.categories[k][1] = 0
		else:
			first, last = expenses[0], expenses[len(expenses)-1]
			start, end = get_first_day(first[4],first[5],first[6],time_unit), get_first_day(last[4],last[5],last[6],time_unit)
			if page is 0:
				for k in self.categories:
					if self.categories[k][2]:
						points, amount, i, iterator = [], 0, 0, self.tab.x_inf
						while iterator <= self.tab.x_sup:
							if iterator < start or iterator > end: points.append(None)
							else:
								while i < len(expenses) and get_first_day(expenses[i][4],expenses[i][5],expenses[i][6],time_unit) <= iterator:
									if k in ["_",expenses[i][3]]: amount += float(expenses[i][2])
									i+= 1
								points.append([amount,iterator])
							iterator = next_item(iterator,time_unit)
						self.categories[k][1] = tuple(points)
			elif page is 1:
				balance = self.balance_combo.get_active()
				for k in self.categories:
					points, i, iterator = [], 0, self.tab.x_inf
					while iterator <= self.tab.x_sup:
						amount = 0
						if iterator < start or iterator > end: points.append(None)
						else:
							while i < len(expenses) and get_first_day(expenses[i][4],expenses[i][5],expenses[i][6],time_unit) < iterator: i += 1
							while i < len(expenses) and get_first_day(expenses[i][4],expenses[i][5],expenses[i][6],time_unit) == iterator:
								if expenses[i][2][0] in ["+-","+","-"][balance] and k in ["_",expenses[i][3]]: amount += float(expenses[i][2])
								i += 1
							points.append([amount,iterator])
						iterator = next_item(iterator,time_unit)
					self.categories[k][1] = tuple(points)
			else:
				balance = self.balance_combo.get_active()
				for k in self.categories:
					amount = 0
					for exp in expenses:
						date = get_first_day(exp[4],exp[5],exp[6],time_unit)
						if date >= self.tab.x_inf and date <= self.tab.x_sup and exp[2][0] in ["+-","+","-"][balance] and k in ["_",exp[3]]: amount += abs(float(exp[2]))
					self.categories[k][1] = amount
		# update y_dist
		if page < 2:
			try:
				coeff = 1 if page is 1 and self.balance_combo.get_active() > 0 else 2
				self.tab.y_dist = coeff*max(1,max(abs(self.categories[k][1][j][0]) for k in self.categories for j in range(self.tab.x_dist+1) if self.categories[k][2] and len(self.categories[k][1]) > 0 and self.categories[k][1][j]))
			except ValueError: self.tab.y_dist = 2
		# update the categories flowbox
		colors = {color: 0 for color in gdk_colors}
		for child in self.categories_flowbox.get_children(): self.categories_flowbox.remove(child)
		for k in self.categories:
			if len(self.categories[k]) is 4 and self.categories[k][3] in colors: colors[self.categories[k][3]] += 1
		for k in sorted(self.categories, key=lambda x: (x != "_",x.lower())):
			if len(self.categories[k]) is 3:
				color = sorted(colors.keys(), key=lambda x: (colors[x],gdk_colors.index(x)))[0]
				self.categories[k].append(color)
				colors[color] += 1
			name = k if k != "_" else _("Total")
			child_obj = CategoryRow(name,self.categories[k][0],self.categories[k][3])
			child_obj.button.connect("color-set",self.update_color,k)
			self.categories_flowbox.insert(child_obj.child,-1)
			if self.categories[k][2]: self.categories_flowbox.select_child(child_obj.child)
		# update points on the graph
		self.tab.update_graph(self.categories)
		self.block_signals = False
	
	# called when the graph is displayed for the first time
	def show_graph_first_time(self):
		self.block_signals = True
		# restore the notebook
		self.notebook.set_current_page(0)
		for box in [self.box1,self.box2,self.box3]:
			children = box.get_children()
			if len(children) > 0: box.remove(children[0])
		# set first and last day (today) of top buttons
		today = datetime.date.today()
		start = today
		if len(self.expenses) > 0:
			first = self.expenses[len(self.expenses)-1]
			start = datetime.date(first[4],first[5],first[6])
		self.period_start_button.set_label(adjust_date(start.year,start.month,start.day))
		self.period_end_button.set_label(adjust_date(today.year,today.month,today.day))
		# set visibility and default value of bottom buttons
		self.time_unit_box.set_visible(True)
		self.balance_box.set_visible(False)
		self.time_unit_combo.set_active(0)
		self.balance_combo.set_active(0)
		# create a new tab object
		self.tab = LineGraph()
		self.box1.add(self.tab.box)
		# connect event signals
		self.tab.drawingarea.connect("configure-event",self.on_drawingarea_configure_event)
		self.tab.drawingarea.connect("motion_notify_event",self.on_drawingarea_motion_notify_event)
		self.tab.drawingarea.connect("leave_notify_event",self.on_drawingarea_leave_notify_event)
		# draw graph
		self.update_graph()
		self.block_signals = False
