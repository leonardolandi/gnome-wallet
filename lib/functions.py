# functions.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from locale import gettext as _

legal_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
months = (	_("January"),
			_("February"),
			_("March"),
			_("April"),
			_("May"),
			_("June"),
			_("July"),
			_("August"),
			_("September"),
			_("October"),
			_("November"),
			_("December")
		)
gdk_colors = (	(32,74,135),	# dark blue from palette
				(164,0,0),		# dark red from palette
				(78,154,6),		# dark green from palette
				(237,212,0),	# medium yellow from palette
				(117,80,123),	# medium purple from palette
				(0,212,170),	# custom lightblue
				(245,121,0),	# medium orange from palette
				(255,85,153),	# custom pink
				(138,226,52),	# light green from palette
				(239,41,41),	# light red from palette
				(0,0,170),		# custom blue
				(252,233,79),	# light yellow from palette
				(173,127,168),	# light purple from palette
				(143,89,2),		# dark brown from palette
				(55,200,113),	# custom green
				(212,0,85),		# custom purple
				(120,33,33),	# custom red
				(114,159,207),	# light blue from palette
			)

#	FORMAT FUNCTIONS

def adjust_date(year,month,day):
	(day,month,year) = (str(day),str(month),str(year))
	if len(day) is 1: day = "0" + day
	if len(month) is 1: month = "0" + month
	while len(year) < 4: year = "0" + year
	return day + "-" + month + "-" + year

def validate_amount(string):
	v = string.replace(",",".").split(".")
	if len(v) > 2: return False
	if len(v[0]) > 0 and (v[0][0] == "-" or v[0][0] == "+"): v[0] = v[0][1:]
	if len(v[0]) > 9: return False
	for i in v:
		if len(i) is 0 or not all((c in "0123456789") for c in i): return False
	if len(v) is 2 and len(v[1]) > 2: return False
	return True

def adjust_amount(string):
	if float(string.replace(",",".")) == 0: return "+0"
	v = string.replace(",",".").split(".")
	negative = (v[0][0] == "-")
	if negative or v[0][0] == "+": v[0] = v[0][1:]
	while len(v[0]) > 0 and v[0][0] == "0": v[0] = v[0][1:]
	if len(v[0]) is 0: v[0] = "0"
	v[0] = "-" + v[0] if negative else "+" + v[0]
	if len(v) is 1 or v[1] == "0" or v[1] == "00": return v[0]
	if len(v[1]) is 1: v[1] = v[1] + "0"
	return ".".join(v)

def validate_search(search,item,reason,amount,category,date):
	if search == "": return True
	if reason and search in item[1].lower(): return True
	if amount and search in item[2]: return True
	if category and search in item[3].lower(): return True
	if date and search in str(item[4]): return True
	if date and search in months[item[5]-1].lower() + " " + str(item[4]): return True
	if date and search in months[item[5]-1][:3].lower() + " " + str(item[4]): return True
	if date and search in str(item[6]) + " " + months[item[5]-1].lower() + " " + str(item[4]): return True
	if date and search in str(item[6]) + " " + months[item[5]-1][:3].lower() + " " + str(item[4]): return True
	return False

#	EXPORT INTO TXT

def export_into_txt(liststore,output):
	(s_date,s_reason,s_category,s_amount,s_total) = (_("Date"),_("Reason"),_("Category"),_("Amount"),_("Total"))
	(l_date,l_reason,l_category,l_amount) = (max(len(s_date),11),len(s_reason),len(s_category),len(s_amount))
	total = 0
	for row in liststore:
		total += float(row[2])
		if len(row[1]) > max(len(s_reason),l_reason): l_reason = len(row[1])
		if len(row[2]) > max(len(s_amount),l_amount): l_amount = len(row[2])
		if len(row[3]) > max(len(s_category),l_category): l_category = len(row[3])
	(strings,lengths) = ([s_date,s_reason,s_category,s_amount],[l_date,l_reason,l_category,l_amount])
	content = s_total + ": " + adjust_amount(str(round(total,2))) + "\n\n+"
	for i in range(4):
		for j in range(lengths[i]): content += "-"
		content += "+"
	content += "\n|"
	for i in range(4):
		content += strings[i]
		for j in range(lengths[i]-len(strings[i])): content += " "
		content += "|"
	content += "\n+"
	for i in range(4):
		for j in range(lengths[i]): content += "-"
		content += "+"
	content += "\n"
	for row in liststore:
		(day,year) = (str(row[6]),str(row[4]))
		if len(day) is 1: day = "0" + day
		while len(year) < 4: year = "0" + year
		content += "|" + day + " " + months[row[5]-1][:3] + " " + year
		for i in range(l_date-11): content += " "
		content += "|" + row[1]
		for i in range(l_reason-len(row[1])): content += " "
		content += "|" + row[3]
		for i in range(l_category-len(row[3])): content += " "
		content += "|" + row[2]
		for i in range(l_amount-len(row[2])): content += " "
		content += "|\n"
	content += "+"
	for i in range(4):
		for j in range(lengths[i]): content += "-"
		content += "+"
	with open(output, "w") as text_file: text_file.write(content)
	text_file.close()

#	DATE FUNCTIONS

def get_first_day(year,month,day,time_unit):
	if time_unit is 0: return datetime.date(year,month,day)
	if time_unit is 1: return datetime.date(year,month,day) - datetime.timedelta(datetime.date(year,month,day).isocalendar()[2] - 1)
	if time_unit is 2: return datetime.date(year,month,1)
	if time_unit is 3: return datetime.date(year,1,1)

def next_item(prev,time_unit,distance=1):
	if not prev: return None
	if time_unit is 0: return prev + datetime.timedelta(distance)
	if time_unit is 1: return prev + datetime.timedelta(7*distance)
	if time_unit is 2:
		(month,year) = (prev.month + distance,prev.year)
		while month > 12: month -= 12; year += 1
		return datetime.date(year,month,1)
	return datetime.date(prev.year + distance,1,1)

def get_label_date(date,time_unit):
	if time_unit is 0: return str(date.day) + " " + months[date.month-1][:3] + " " + str(date.year)
	if time_unit is 2: return months[date.month-1][:3] + " " + str(date.year)
	if time_unit is 3: return str(date.year)
	date = get_first_day(date.year,date.month,date.day,1)
	next = next_item(date,time_unit)
	if date.month == next.month: return str(date.day) + " - " + str(next.day) + " " + months[date.month-1][:3] + " " + str(date.year)
	if date.year == next.year: return str(date.day) + " " + months[date.month-1][:3] + " - " + str(next.day) + " " + months[next.month-1][:3] + " " + str(date.year)
	return str(date.day) + " " + months[date.month-1][:3] + " " + str(date.year) + " - " + str(next.day) + " " + months[next.month-1][:3] + " " + str(next.year)

#	SORT EXPENSES LIST

def sort_list_by_order(expenses,order):
	if order is 0: return tuple(sorted(expenses, key=lambda x: (x[4],x[5],x[6],x[0]), reverse=True))
	if order is 1: return tuple(sorted(expenses, key=lambda x: (x[4],x[5],x[6],x[0])))
	if order is 2: return tuple(sorted(expenses, key=lambda x: (x[1].lower(),-x[4],-x[5],-x[6],-x[0])))
	if order is 3: return tuple(sorted(expenses, key=lambda x: (x[1].lower(),x[4],x[5],x[6],x[0]), reverse=True))
	if order is 4: return tuple(sorted(expenses, key=lambda x: (float(x[2]),x[4],x[5],x[6],x[0]), reverse=True))
	if order is 5: return tuple(sorted(expenses, key=lambda x: (float(x[2]),-x[4],-x[5],-x[6],-x[0])))
	if order is 6: return tuple(sorted(expenses, key=lambda x: (x[3].lower(),-x[4],-x[5],-x[6],-x[0])))
	else: return tuple(sorted(expenses, key=lambda x: (x[3].lower(),x[4],x[5],x[6],x[0]), reverse=True))
