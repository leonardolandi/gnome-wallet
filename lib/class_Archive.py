# class_Archive.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sqlite3 import connect, OperationalError
from subprocess import check_output, CalledProcessError

def new_archive_file(filepath,password):
	connection = connect(filepath)
	cursor = connection.cursor()
	cursor.execute("CREATE TABLE IF NOT EXISTS wallet (reason TEXT, amount TEXT, category TEXT, year INTEGER, month INTEGER, day INTEGER)")
	connection.commit()
	connection.close()
	if len(password) > 0: check_output("gpg --batch --yes --cipher-algo AES --passphrase " + password + " --symmetric -o " + filepath + ".gpg " + filepath + "; rm " + filepath, shell = True)

def match_archive_password(filepath,password):
	try:
		check_output("echo '" + password + "' | gpg --batch --passphrase-fd 0 -o /dev/null " + filepath + ".gpg", shell = True)
		return True
	except CalledProcessError: return False

class Archive:

	def __init__(self,filepath,name,password):
		self.filepath = filepath
		self.name = name
		self.password = password

	def encrypt(self,password): check_output("gpg --batch --yes --cipher-algo AES --passphrase " + password + " --symmetric -o " + self.filepath + ".gpg " + self.filepath + "; rm " + self.filepath, shell = True)

	def decrypt(self): check_output("gpg --batch --yes --passphrase " + self.password + " -o " + self.filepath + " " + self.filepath + ".gpg; rm " + self.filepath + ".gpg", shell = True)

	def insert(self,values,many=False):
		res = False
		if len(self.password) > 0: self.decrypt()
		connection = connect(self.filepath)
		cursor = connection.cursor()
		try:
			if many:
				cursor.executemany("INSERT INTO wallet VALUES (?,?,?,?,?,?)",values)
				res = True
			else:
				cursor.execute("INSERT INTO wallet VALUES (?,?,?,?,?,?)",values)
				res = cursor.lastrowid
			connection.commit()
		except OperationalError: pass
		connection.close()
		if len(self.password) > 0: self.encrypt(self.password)
		return res

	def update(self,values,ids):
		res = False
		if len(self.password) > 0: self.decrypt()
		connection = connect(self.filepath)
		cursor = connection.cursor()
		try:
			string, insertion = "UPDATE wallet SET ", tuple()
			for key in values:
				string += key + " = ?, "
				insertion += values[key],
			string = string[:-2]
			string += " WHERE rowid IN (" + "?, " * len(ids)
			string = string[:-2]
			string += ")"
			insertion += ids
			cursor.execute(string,insertion)
			connection.commit()
			res = True
		except OperationalError: pass
		connection.close()
		if len(self.password) > 0: self.encrypt(self.password)
		return res

	def delete(self,ids):
		res = False
		if len(self.password) > 0: self.decrypt()
		connection = connect(self.filepath)
		cursor = connection.cursor()
		try:
			string = "DELETE FROM wallet WHERE rowid IN (" + "?, " * len(ids)
			string = string[:-2]
			string += ")"
			cursor.execute(string,ids)
			connection.commit()
			res = True
		except OperationalError: pass
		connection.close()
		if len(self.password) > 0: self.encrypt(self.password)
		return res

	def select(self):
		res = False
		if len(self.password) > 0: self.decrypt()
		connection = connect(self.filepath)
		cursor = connection.cursor()
		try:
			rows = cursor.execute("SELECT rowid,reason,amount,category,year,month,day FROM wallet")
			res = tuple(row for row in rows)
			connection.commit()
		except OperationalError: pass
		connection.close()
		if len(self.password) > 0: self.encrypt(self.password)
		return res

	def rename(self,new_filepath,new_name):
		self.filepath = new_filepath
		self.name = new_name

	def reset_password(self,new_password):
		if len(self.password) is 0 and len(new_password) > 0: self.encrypt(new_password)
		elif len(self.password) > 0 and len(new_password) is 0: self.decrypt()
		elif len(self.password) > 0 and len(new_password) > 0: self.decrypt(); self.encrypt(new_password)
		self.password = new_password
