# tab_DotsGraph.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk, Gdk
from math import pi
import cairo

class DotsGraph:

	def __init__(self,data = None):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/tab_LineGraph.glade")
		builder.set_translation_domain("wallet")

		self.box = builder.get_object("box")
		self.drawingarea = builder.get_object("drawingarea")
		self.y_scale = builder.get_object("y_scale")
		self.x_label = builder.get_object("x_label")
		self.y_label = builder.get_object("y_label")
		self.drawingarea.connect("draw",self.on_drawingarea_draw)
		self.drawingarea.set_events(Gdk.EventMask.POINTER_MOTION_MASK | Gdk.EventMask.LEAVE_NOTIFY_MASK)
		self.balance = 0
		self.surface, self.context = None, self.drawingarea.get_style_context()
		if data: self.import_data(data)
		else: self.x_inf, self.x_sup, self.x_dist, self.y_dist = None, None, None, None

	def on_drawingarea_draw(self,widget,context):
		context.set_source_surface(self.surface,0,0)
		context.paint()

	def draw_background(self,context,width,height):
		context.set_source_rgb(1,1,1)
		context.rectangle(0,0,width,height)
		context.fill()
		context.stroke()

	def draw_zero_line(self,context,width,height):
		context.set_line_width(0.1)
		context.set_source_rgb(0,0,0)
		context.move_to(0,height/2)
		context.line_to(width,height/2)
		context.stroke()

	def draw_dots(self,categories,context,zero,x_div,y_div):
		for k in categories:
			if categories[k][2]:
				points, color = categories[k][1], categories[k][3]
				i = 0
				while i < len(points) and points[i] is None: i += 1
				while i < len(points) and points[i] is not None:
					if points[i][0] != 0:
						x, y = x_div*i, zero - y_div*points[i][0]*0.95
						context.move_to(x,zero)
						context.set_source_rgb(0,0,0)
						context.line_to(x,y)
						context.stroke()
						context.set_source_rgb(color[0]/255,color[1]/255,color[2]/255)
						context.arc(x,y,3,0,2*pi)
						context.fill()
					i += 1

	def update_graph(self,categories):
		if self.surface is None: return
		context, width, height = cairo.Context(self.surface), self.surface.get_width(), self.surface.get_height()
		# draw background
		self.draw_background(context,width,height)
		if self.balance is 0: self.draw_zero_line(context,width,height)
		if self.x_dist is 0:
			self.surface.flush()
			self.drawingarea.queue_draw()
			return
		# draw main content
		x_div, y_div = width/self.x_dist, height/self.y_dist
		context.set_line_width(0.1)
		if self.balance is 0: self.draw_dots(categories,context,height/2,x_div,y_div)
		elif self.balance is 1: self.draw_dots(categories,context,height,x_div,y_div)
		else: self.draw_dots(categories,context,height,x_div,-y_div)
		# flush
		self.surface.flush()
		self.drawingarea.queue_draw()

	def export_data(self): return {
		"x_inf": self.x_inf,
		"x_sup": self.x_sup,
		"x_dist": self.x_dist,
		"y_dist": self.y_dist }

	def import_data(self,data):
		self.x_inf = data["x_inf"] # first date, date format
		self.x_sup = data["x_sup"] # last date, date format
		self.x_dist = data["x_dist"] # number of notches on the x axis
		self.y_dist = data["y_dist"] # max distance between amount values
