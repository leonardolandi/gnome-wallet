# dialog_RemoveArchive.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from locale import gettext as _

class RemoveArchive:

	def __init__(self,main_window,name,basename):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/dialog_RemoveArchive.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		self.window = builder.get_object("window")
		self.window.set_transient_for(main_window)
		if name is None:
			self.window.set_title(_("Existing file. Overwrite?"))
			builder.get_object("label").set_text(_("File") + " " + basename + " " + _("already exists inside the selected folder.\nOverwrite it?"))
			builder.get_object("ok_button").set_label(_("Overwrite"))
		else:
			self.window.set_title(_("Remove archive") + " \"" + name + "\"")
			builder.get_object("label").set_text(_("Are you sure?") + "\n" + _("Deleted archive will not be recoverable"))
			builder.get_object("ok_button").set_label(_("Remove"))

	def on_window_key_release_event(self,widget,event):
		if event.keyval == 65293: widget.emit("response",1)
