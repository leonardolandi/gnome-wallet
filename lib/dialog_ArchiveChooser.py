# dialog_ArchiveChooser.py
# This file is part of Wallet
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from locale import gettext as _

class ArchiveChooser:

	def __init__(self,main_window,remove_action):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/dialog_ArchiveChooser.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("wallet")

		self.window = builder.get_object("window")
		self.listbox = builder.get_object("listbox")
		self.unavailable_box = builder.get_object("unavailable_box")
		self.check_button = builder.get_object("check_button")
		self.revealer = builder.get_object("revealer")

		self.checked = False
		self.window.set_transient_for(main_window)
		button = builder.get_object("ok_button")
		if remove_action:
			self.window.set_title(_("Choose an archive to remove"))
			button.set_label(_("Remove"))
			button.get_style_context().add_class("destructive-action")
		else:
			self.window.set_title(_("Choose the archive to import data"))
			button.set_label(_("Import"))
			button.get_style_context().add_class("suggested-action")
			self.check_button.set_visible(True)
			self.revealer.set_reveal_child(False)

	def on_window_key_release_event(self,widget,event):
		if event.keyval == 65293 and not self.check_button.is_focus(): widget.emit("response",1)

	def on_check_button_toggled(self,widget):
		self.checked = not self.checked
		self.revealer.set_reveal_child(widget.get_active())

	def on_listbox_hide(self,widget):
		self.unavailable_box.set_visible(True)
		self.check_button.set_visible(False)
		self.revealer.set_reveal_child(False)
