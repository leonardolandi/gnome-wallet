prefix = /usr

APPDIR = $(prefix)/lib/wallet
BINDIR = $(prefix)/bin
ICONDIR = $(prefix)/share/icons/hicolor
DESKTOPDIR = $(prefix)/share/applications
LICENSEDIR = $(prefix)/share/licenses/wallet
LANGDIR = $(prefix)/share/locale

check:

	@# check if packages are installed
	@for i in python3 msgfmt; do \
		if [ ! $$(which $$i 2> /dev/null) ]; then \
			echo ""; \
			echo \'$$i\' is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			false; \
		fi; \
	done;

	@# check for python modules
	@for i in os cairo datetime gi locale subprocess shutil sqlite3; do \
		python3 -c "import "$$i 2> /dev/null; \
		if [ $$? = 1 ]; then \
			echo ""; \
			echo python \'$$i\' module is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			false; \
		fi; \
	done;

	@# check for optional dependencies
	@for i in iconv enscript; do \
		if [ ! $$(which $$i 2> /dev/null) ]; then \
			echo ""; \
			echo \'$$i\' is an optional dependency that is not installed on your system.; \
			echo \'Export into PDF\' functionality will not be available without it.; \
			echo ""; \
		fi; \
	done;

install:

	@# check if packages are installed
	@echo -ne Checking for dependencies...;
	@for i in python3 msgfmt; do \
		if [ ! $$(which $$i 2> /dev/null) ]; then \
			echo -e "\n"; \
			echo ERROR: \'$$i\' is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			exit 1; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# check for python modules
	@echo -ne Checking for python modules...;
	@for i in os cairo datetime gi locale subprocess shutil sqlite3; do \
		python3 -c "import "$$i 2> /dev/null; \
		if [ $$? = 1 ]; then \
			echo -e "\n"; \
			echo ERROR: python \'$$i\' module is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			exit 1; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# copy python files
	@echo -ne Installing libraries...;
	@mkdir -p $(APPDIR)/python;
	@cp "$(CURDIR)"/lib/* $(APPDIR)/python/;
	@sed -i "s|LANGUAGE_FOLDER|\"$(LANGDIR)\"|g" $(APPDIR)/python/translation.py

	@# copy glade files
	@mkdir -p $(APPDIR)/glade;
	@cp "$(CURDIR)"/ui/* $(APPDIR)/glade/;
	@echo -ne " "ok'\n';

	@# copy icons
	@echo -ne Installing icons...;
	@for i in 16x16 32x32 48x48 64x64 128x128 256x256 scalable symbolic; do \
		mkdir -p $(ICONDIR)/$$i/apps; \
		cp "$(CURDIR)"/icons/$$i/* $(ICONDIR)/$$i/apps/; \
	done;
	@echo -ne " "ok'\n';

	@# copy languages
	@echo -ne Installing languages...;
	@for i in "$(CURDIR)"/po/*.po; do \
		h=$${i%.po}; \
		j=$${h##*/}; \
		mkdir -p $(LANGDIR)/$$j/LC_MESSAGES; \
		msgfmt "$$i" -o $(LANGDIR)/$$j/LC_MESSAGES/wallet.mo; \
	done;
	@echo -ne " "ok'\n';

	@# create executable file
	@echo -ne Installing executable...;
	@mkdir -p $(BINDIR);
	@echo 'python3 '$(APPDIR)'/python/main.py' > $(BINDIR)/wallet;
	@chmod +x $(BINDIR)/wallet;
	@echo -ne " "ok'\n';

	@# install desktop launcher
	@echo -ne Installing desktop launcher...;
	@mkdir -p $(DESKTOPDIR);
	@desktop-file-install "$(CURDIR)"/Wallet.desktop;
	@update-desktop-database;
	@echo -ne " "ok'\n';

	@# copy license
	@echo -ne Installing license...;
	@mkdir -p $(LICENSEDIR);
	@cp "$(CURDIR)"/LICENSE $(LICENSEDIR)/;
	@echo -ne " "ok'\n';

	@# update icon cache
	@echo -ne Updating icon cache...;
	@gtk-update-icon-cache -q -t -f $(ICONDIR);
	@echo -ne " "ok'\n';

	@# create folder for archives in users home
	@echo -ne Creating users folders...;
	@for i in /home/*/; do \
		if [ -w "$$i" ]; then \
			mkdir -p $$i.wallet; \
			chmod 777 -R $$i.wallet; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# check for optional dependencies
	@echo -ne Checking for optional dependencies...;
	@if [ ! $$(which iconv 2> /dev/null) ]; then \
		if [ ! $$(which enscript 2> /dev/null) ]; then \
			echo -e "\n"; \
			echo WARNING: \'iconv\' and \'enscript\' are optional dependencies that are not installed on your system.; \
			echo \'Export into PDF\' functionality will not be available without them.; \
			echo ""; \
		else \
			echo -e "\n"; \
			echo WARNING: \'iconv\' is an optional dependency that is not installed on your system.; \
			echo \'Export into PDF\' functionality will not be available without it.; \
			echo ""; \
		fi; \
	elif [ ! $$(which enscript 2> /dev/null) ]; then \
		echo -e "\n"; \
		echo WARNING: \'enscript\' is an optional dependency that is not installed on your system.; \
		echo \'Export into PDF\' functionality will not be available without it.; \
		echo ""; \
	else \
		echo -ne " "ok'\n'; \
	fi;

	@# display an ending message
	@echo Wallet was successfully installed.

uninstall:

	@rm -rf $(APPDIR);
	@rm -rf $(BINDIR)/wallet;
	@rm -f $(DESKTOPDIR)/Wallet.desktop;
	@for i in 16x16 32x32 48x48 64x64 128x128 256x256; do rm -f $(ICONDIR)/$$i/apps/wallet.png; done;
	@rm -f $(ICONDIR)/scalable/apps/wallet.svg;
	@rm -f $(ICONDIR)/symbolic/apps/wallet-symbolic.svg;
	@rm -f $(ICONDIR)/symbolic/apps/wallet-circle-symbolic.svg;
	@rm -f $(ICONDIR)/symbolic/apps/wallet-lines-symbolic.svg;
	@rm -f $(ICONDIR)/symbolic/apps/wallet-points-symbolic.svg;
	@rm -rf $(LICENSEDIR);
	@for i in "$(CURDIR)"/po/*.po; do \
		h=$${i%.po}; \
		j=$${h##*/}; \
		rm -f $(LANGDIR)/$$j/LC_MESSAGES/wallet.mo; \
	done;
	@echo Wallet was successfully uninstalled.;
